RuleSet: International(version)
* insert SNOMED_CT(900000000000207008, {version})

CodeSystem: SNOMED_CT_International_20230731
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 900000000000207008-20230731
* insert International(20230731)

CodeSystem: SNOMED_CT_International_20240101
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 900000000000207008-20240101
* insert International(20240101)

CodeSystem: SNOMED_CT_International_20240701
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 900000000000207008-20240701
* insert International(20240701)

CodeSystem: SNOMED_CT_International_20250101
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 900000000000207008-20250101
* insert International(20250101)