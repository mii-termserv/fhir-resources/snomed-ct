RuleSet: Germany(version)
* insert SNOMED_CT(11000274103, {version})

CodeSystem: SNOMED_CT_GermanyEdition_20231115
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 11000274103-20231115
* insert Germany(20231115)

CodeSystem: SNOMED_CT_GermanyEdition_20240515
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 11000274103-20240515
* insert Germany(20240515)

CodeSystem: SNOMED_CT_GermanyEdition_20241115
Title: "SNOMED CT core"
Description: "SNOMED CT is the most comprehensive and precise clinical health terminology product in the world, owned and distributed around the world by The International Health Terminology Standards Development Organisation (IHTSDO)."
Id: 11000274103-20241115
* insert Germany(20241115)