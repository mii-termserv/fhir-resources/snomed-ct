Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: SNOMED_CT(module, version)
* ^url = "http://snomed.info/sct"
* ^version = "http://snomed.info/sct/{module}/version/{version}"
* ^status = #active
* ^copyright = "© 2002-2016 International Health Terminology Standards Development Organisation (IHTSDO). All rights reserved. SNOMED CT®, was originally created by The College of American Pathologists. \"SNOMED\" and \"SNOMED CT\" are registered trademarks of the IHTSDO https://www.snomed.org/get-snomed"
* ^caseSensitive = true
* ^valueSet = "http://snomed.info/sct/{module}/version/{version}?fhir_vs"
* ^content = #not-present
* ^hierarchyMeaning = #is-a
* ^meta.tag[+].system = $sutermserv_project
* ^meta.tag[=].code = #international
* ^meta.tag[=].display = "International standard terminology"
* ^meta.tag[+].system = $sutermserv_dataset
* ^meta.tag[=].code = #snomed-ct
* ^meta.tag[=].display = "Versions of SNOMED CT"
* ^meta.tag[+].system = $sutermserv_license
* ^meta.tag[=].code = #https://mii-termserv.de/licenses#snomed-ct
* ^meta.tag[=].display = "SNOMED CT license"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/snomed-ct"
